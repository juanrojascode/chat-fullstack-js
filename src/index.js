const path = require('path');
const express = require("express");
const app = express();

const mongoose = require('mongoose');

const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

/* DB connection */
mongoose.connect('mongodb://127.0.0.1/chattest', {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
.then(db => console.log('DB is connected.'))
.catch(err => console.log(err));

/* setting */
app.set('port', process.env.PORT || 3000);

require('./sockets')(io);

/* Static files */
app.use( express.static( path.join(__dirname,'public') ));

/* Starting the server */
server.listen(app.get('port'), () => {
  console.log("server on port " + app.get('port'));
});
