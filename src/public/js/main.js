$(document).ready(function () {
  const socket = io();

  /* Init toastr options */
  toastr.options = {
    "closeButton": false,
    "positionClass": "toast-bottom-center",
  }

  const chatBody = $(".chatBox--item-body");
  socket.on("load old msgs", msgs => {
    for (let i = 0; i < msgs.length; i++) {
      chatBody.append(`<p class="text-muted"><b>${msgs[i].username}:</b> ${msgs[i].msg}</p>`);
    }
    chatBody.append('<hr>');
    scrollDiv();
  });

  if (sessionStorage.length >= 1 && sessionStorage.username != "undefined") {
    loggedIn(socket, sessionStorage.username);
    $("#btnLogout").show();
  }
  
  /* Init functions */
  loginForm(socket);
  registerForm(socket);
  chatFun(socket);
  logout();
  
});

function chatFun(socket) {
  const chatForm = $("#chatBox--item-inputMsn");
  const chatBody = $(".chatBox--item-body");
  const userList = $(".chatBox--users-body");
  const chatMsn = $(".chatBox--item-msn");

  /* Evento que envía el mensaje al servidor para que sea enviado a todos los clientes */
  $(chatForm).submit((e) => {
    e.preventDefault();
    socket.emit("send message", chatMsn.val()); /* Envía al servidor el mensaje escrito */
    chatMsn.val(""); /* Limpia el campo */
    scrollDiv();
  });

  /* Evento que escucha que llega al servidor y lo muestra cuerpo del chat */
  socket.on("new message", (data) => {
    let user = data.username; 
    if (data.typeuser == "moderador") {
      user += '<span class="badge bg-warning text-dark">Mod</span>';
    }
    /* Imprime el usuario y el mensaje enviado */
    chatBody.append(`<p><b>${user}:</b> ${data.msg}</p>`);
  });
  
  /* Evento para escuchar al servidor y enlistas los nuevos clientes entrantes */
  socket.on("users online", (data) => {
    let html = "";
    for (let i = 0; i < data.length; i++) {
      if (data[i].type == "moderador") {
        html += `<span class="badge rounded-pill bg-success">${data[i].name}<span class="badge bg-warning text-dark">Mod</span></span>`;
      }else{
        html += `<span class="badge rounded-pill bg-success">${data[i].name}</span>`;
      }
    }
    userList.html(html);
  });
}

function registerForm(socket) {
  $("#sendInfoRegister").click((e) => {
    e.preventDefault();

    const name = $("#name").val();
    const username = $("#username").val();
    const password = $("#password").val();
    let typeuser = $("#typeUser").val();

    if (name == "" || username == "" || password == "") {
      $('.error-all').addClass('d-block');
      toastr.error('Todos los campos son necesarios');
    } else {
      socket.emit("valid username", username, (data) => {
        if (data) {
          toastr.error('El usuario ya existe, intente con otro.');
        } else {
          let data = {
            name,
            username,
            password,
            typeuser
          };
          socket.emit("create user", data);          
          $('#modalRegisterForm').modal('hide');
          sessionStorage.setItem('username', username);
          loggedIn(socket, username);
          Swal.fire({
            icon: 'success',
            title: '¡Bienvenido!',
            text: 'Usuario creado satisfactoriamente',
            showConfirmButton: false,
            timer: 2500
          });
          setTimeout(() => {
            window.location.reload();
          }, 2500);
        }
      });
    }
  });
}

function loginForm(socket) {
  const logFormBtn = $("#loginBox--btn");
  
  $(logFormBtn).click(function (e) {
    e.preventDefault();
    const logUserName = $("#logUserName").val();
    const logUserPass = $("#logUserPass").val();

    if (logUserName == "" || logUserPass == "") {
      toastr.error('Ingrese el usuario y contraseña.');
    }else{
      const userData = {
        logUserName,
        logUserPass
      }
      socket.emit("valid login", userData , (data) => {
        if (data) {
          loggedIn(socket, logUserName);
        } else {
          toastr.error('Usuario y/o contraseña incorrectos.');
        }
      });
    }
  });
}

function loggedIn(socket, user) {
  const logUserName = $("#logUserName");

  /* Evento que crea un nuevo cliente y en caso de no estar logeado entrará a la clase */
  socket.emit("new user", user, (data) => {
    if (data) {
      $(".chatBox").addClass("d-flex");
      $(".loginBox").hide();
      sessionStorage.setItem('username', user);
      $("#btnLogout").show();
    } else {
      toastr.error('¡<b>'+user+'</b> ya está logeado!</div>');
      logUserName.val("");
    }
  });
}

function logout() {
  $("#btnLogout").click(function (e) { 
    /* e.preventDefault(); */
    /* $("#btnLogout").hide(); */
    sessionStorage.clear();
    window.location.reload();
  });
}

function scrollDiv(){
  $(".chatBox--item-body").animate({ scrollTop: $('.chatBox--item-body')[0].scrollHeight}, 1000);
}