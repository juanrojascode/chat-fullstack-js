const Msgs = require('./models/Messages');
const Users = require('./models/Users');

module.exports = function (io) {
    let usersOnline = [];

    /* Establece una conexión constante */
    io.on('connection', async socket => {

        let msgss = await Msgs.find();
        socket.emit('load old msgs', msgss);

        /* Crea un nuevo usuario, lo agrega a un arreglo
        en caso de ya estar logueado el usuario, retornará false */
        socket.on('new user', async (data, callBack) => {
            if (usersOnline.indexOf(data) != -1) {
                callBack(false);
            } else {
                callBack(true);
                const usertype = await Users.findOne({ "username": data });
                socket.userName = data; /* Guarda el nombre en el objeto socket */
                socket.userType = usertype.typeuser;
                const dataConst = { "name": socket.userName, "type": socket.userType};
                usersOnline.push(dataConst); /* Guarda el nombre en el arreglo */
                updateUsers(); /* Actualiza la lista de usuarios activos */
            }
        });

        /* Emite el mensaje recibido por el usuario a todos los usuarios activos */
        socket.on('send message', async data => {
            /* Almacenar de forma asyncrona en la DB */
            let user = socket.userName;
            if (socket.userType == "moderador") {
                user += '<span class="badge bg-warning text-dark">Mod</span>';
            }
            let newMsg = new Msgs({
                username: user,
                msg: data
            });
            await newMsg.save();

            /* Envía el nombre del usuario y su respectivo mensaje */
            io.sockets.emit('new message', {
                username: socket.userName,
                msg: data,
                typeuser: socket.userType
            });
        });

        /* Remover usuarios que salen del chat */
        socket.on('disconnect', data => {
            if (!socket.userName) return;
            /* Elimina el usuario que salió de la sesión */
            usersOnline.splice(usersOnline.indexOf(socket.userName), 1);
            updateUsers(); /* Actualiza la lista de usuarios activos */
        })

        /* Actualiza la lista de usuarios en todos los clientes */
        function updateUsers() {
            io.sockets.emit('users online', usersOnline);
        }

        socket.on('create user', async data => {
            /* Crea un nuevo usuario en la DB */
            let newUser = new Users({
                name: data.name,
                username: data.username,
                password: data.password,
                typeuser: data.typeuser
            });
            await newUser.save();
        });

        socket.on('valid username', async (data, callBack) => {
            const nameuserValid = await Users.findOne({ "username": data });
            if (nameuserValid) {
                callBack(true);
            } else {
                callBack(false);
            }
        });

        socket.on('valid login', async (data, callBack) => {
            const userValid = await Users.findOne({ "username": data.logUserName, "password": data.logUserPass });
            if (userValid) {
                callBack(true);
            } else {
                callBack(false);
            }
        });

    });
};
