# Chat FullStack JS

Este proyecto se realiza con el fin de presentar la prueba para el cargo de Desarrollador FullStack JS Junior para la empresa KUEPA en la ciudad de Bogotá, Colombia.

Como objetivo específico: Desarrollar una herramienta de chat en las clases virtuales que permita la interacción entre los participantes y el moderador de la clase.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Pre-requisitos 📋

Para obtener un entorno de desarrollo óptimo y correr satisfactoriamente esta aplicación debe disponer de:

```
un editor de código (VSC, SublimeText, Atom, etc...)
npm
mongoDB
```

### Instalación 🔧

Para iniciar la instalación de esta aplicación, primero deberá descargar una copia de este proyecto en su máquina, para lograr esto, utilice el comando común para esto

```
git clone https://juanrojascode@bitbucket.org/juanrojascode/chat-fullstack-js.git
```

Una vez tenga el proyecto en su máquina, ábralo con su editor de código favorito o navegue hasta la carpeta que acaba de crearse (chat-fullstack-js) y ejecute npm para descargar los módulos Node.JS necesarios, para esto, ejecute en una ventana de comandos:

```
npm install
```

Para finalizar, se ha configurado un comando para ejecutar el servidor de una forma más fácil

**Importante:** recuerde tener encendido y funcionando el motor de base de datos MongoDB.

```
npm run dev
```

y ahora abra su navegador favorito y navegue a la siguiente url

```
http://localhost:3000/
```

Como es la primera vez que ejecuta la aplicación, deberá registrarse en la aplicación.

## Construido con 🛠️

Menciona las herramientas que utilizaste para crear tu proyecto

* [Node.js](https://nodejs.org/es/docs/) - Entorno de ejecución para JavaScript
* [express](https://expressjs.com/es/) - Infraestructura de aplicaciones web Node.js
* [socket.io](https://socket.io/docs/v4) - Biblioteca que permite la comunicación en tiempo real, bidireccional y basada en eventos entre el navegador y el servidor.
* [Mongoose](https://www.npmjs.com/package/mongoose) - Herramienta de modelado de objetos de MongoDB
* [Nodemon](https://www.npmjs.com/package/nodemon) - Herramienta de reincio automático
* [Bootstrap](https://getbootstrap.com/docs/5.0/getting-started/introduction/) - Framework front-end
* [jQuery](https://api.jquery.com/) - Biblioteca de JavaScript

## Autores ✒️

Menciona a todos aquellos que ayudaron a levantar el proyecto desde sus inicios

* **Juan David Rojas** - *Desarrollo y documentación*